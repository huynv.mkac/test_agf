---
HuyNV
AGV/AGF RnD MKAC
March 2021
---

# README

## Test AGF

Cấu trúc thư mục trong package

```bash

├── CMakeLists.txt
├── include
│   └── test_agf
├── package.xml
├── readme.md
├── results
│   ├── ...
│   ├── lastest_log.txt
│   ├── lastest_plan.txt
│   ├── position.png
│   └── vel.png
├── scripts
│   ├── log_plan.py
│   └── show_plt.py
└── src
```

## Chi tiết

- `scripts/log_plan.py`: log lại các thông tin:
  - MkacPlanner/plan trong file `results/*_plan.txt`
  - Vận tốc dài, góc lái điều khiển từ `/cmd_vel`, vận tốc dài và góc lái phản hồi của forklift, vị trí forklift vào file `results/*_log.txt`

- `scripts/show_plt.py`: Vẽ đồ thị kết quả log
  - Mặc định sẽ vẽ từ file log mới nhất `lastest_*`
  - Nếu thêm arg là thời gian (ví dụ `./show_plt.py 210308_143619`) sẽ vẽ đồ thị kq đc lưu trong file log và cmd tương ứng