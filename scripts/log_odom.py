#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry # log odometry
from geometry_msgs.msg import Twist, Pose # log cmd_vel
import datetime
import  rospkg
from nav_msgs.msg import Path
from std_msgs.msg import Float64
import sys, os
from shutil import copyfile, copy
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import tf
pkg_path = rospkg.RosPack().get_path("test_agf")
results_path = os.path.join(pkg_path, "results")


class LogTest():
    def __init__(self):
        rospy.init_node("logging")
        timenow = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
        rospy.loginfo("Init node logging. Log to {}_*.txt".format(timenow))
        # fileName = os.path.join(results_path, str(timenow) + ".txt")
        self.planFileName = os.path.join(results_path, timenow + "_plan.txt")
        self.cmdFileName = os.path.join(results_path, timenow + "_log.txt")
        self.lastestPlanFileName = os.path.join(results_path, "lastest_plan.txt")
        self.lastestLogFileName = os.path.join(results_path, "lastest_log.txt")
        self.navLogFileName = os.path.join(results_path, "nav.txt")
        self.odomLogFileName = os.path.join(results_path, "odom.txt")
        self.odomTestLogFileName = os.path.join(results_path, "odomTest.txt")

        self.navLogFile = open(self.navLogFileName, 'w+')
        self.odomLogFile = open(self.odomLogFileName, 'w+')
        self.odomTestLogFile = open(self.odomTestLogFileName, 'w+')
        # self.cmdVelFile = open(timenow, "_cmdVel.txt"))
        # self.odomFile = open(timenow, "_odom.txt"))

        self.start_log = False
        self.stop_log = False
        self.planed = False
        self.cmdVel = [0.0, 0.0] # [vx, phi]
        self.fbVel = 0.0 # vx
        self.steeringAngleFB = 0.0
        self.odomPose = [0.0, 0.0] # x, y
        self.odomTestPose = [0.0, 0.0]
        self.navPose = [0.0, 0.0]
        self.timeout = 2
        self.lastCmd = rospy.Time()
        self.listener = tf.TransformListener()

        rospy.Subscriber("/move_base/MkacPlanner/plan", Path, self.planCB)
        rospy.Subscriber("/cmd_vel", Twist, self.cmdCB)
        rospy.Subscriber("/odom", Odometry, self.odomCB)
        rospy.Subscriber("/steering_angle", Float64, self.steeringCB)
        rospy.Subscriber("/robot_pose", Pose, self.rbPoseCB)
        rospy.Subscriber("/odom_test", Odometry, self.odomTestCB)
        rospy.Subscriber('/nav350laser/odom', Odometry, self.laser_odom_cb)

        self.navLogFile.write("{}\nx\t\ty--NAV---\n".format(timenow))
        self.odomLogFile.write("{}\nx\t\ty--ODOM---\n".format(timenow))
        self.odomTestLogFile.write("{}\nx\t\ty--ODOMTEST---\n".format(timenow))

        while not rospy.is_shutdown():
            rospy.sleep(0.05) # rate 10Hz
            if self.start_log and not self.stop_log:
                timenow = rospy.Time.now()
                self.lastestLogFile.write("{} {} {} {} {} {} {}\n".format(timenow.to_sec(), self.cmdVel[0], self.cmdVel[1], self.fbVel, self.steeringAngleFB, self.odomPose[0], self.odomPose[1]))

                if (timenow - self.lastCmd).to_sec() > 3:
                    self.stop_log = True

            if self.stop_log:
                self.lastestPlanFile.close()
                self.lastestLogFile.close()
                rospy.loginfo("Close log file!")
                copy(self.lastestLogFileName, self.cmdFileName)
                copy(self.lastestPlanFileName, self.planFileName)
                os.system(pkg_path + '/scripts/show_plt.py')
                rospy.signal_shutdown("shut down node")


    def laser_odom_cb(self, msg):
        try:
            (trans,rot) = self.listener.lookupTransform('/odom', '/footprint_from_nav', rospy.Time(0))
            # print("laser_odom: x {} y {}".format(trans[0], trans[1]))
            self.navLogFile.write("{} {} {}\n".format(rospy.Time.now().to_sec(), trans[0], trans[1]))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

    def odomCB(self, msg):
        self.odomLogFile.write("{} {} {}\n".format(rospy.Time.now().to_sec(), msg.pose.pose.position.x, msg.pose.pose.position.y))

    def odomTestCB(self, msg):
        self.odomTestLogFile.write("{} {} {}\n".format(rospy.Time.now().to_sec(), msg.pose.pose.position.x, msg.pose.pose.position.y))

    def planCB(self, path):
        if not self.planed:
            self.planed = True

        for pose in path.poses:
            self.lastestPlanFile.write("{} {}\n".format(pose.pose.position.x, pose.pose.position.y))

    def cmdCB(self, msg):
        if self.planed:
            self.start_log = True

        self.cmdVel = [msg.linear.x, msg.angular.z]
        self.lastCmd = rospy.Time.now()

    def steeringCB(self, msgSteer):
        self.steeringAngleFB = msgSteer.data

    def rbPoseCB(self, msgPose):
        self.odomPose = [msgPose.position.x, msgPose.position.y]

if __name__ == "__main__":
    log = LogTest()