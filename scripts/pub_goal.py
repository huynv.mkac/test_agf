#! /usr/bin/env python
import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped, Pose
from tf.transformations import euler_from_quaternion, quaternion_from_euler, euler_from_quaternion
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from ultils import *
import math

class PublishGoalTest():
    def __init__(self, pose_type):
        rospy.init_node("pub_test_goal")
        if pose_type == "amcl":
            rospy.loginfo("Ussing amcl_pose to update amcl")
            rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self.amclPoseCB)
        elif pose_type == "robot_pose":
            rospy.loginfo("Ussing robot_pose to update robot pose")
            rospy.Subscriber('/robot_pose', Pose, self.robotPoseCB)

        self.currentPose = RBPose()
        self.goal = RBPose()
        goal_pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)

        while not rospy.is_shutdown():
            rospy.sleep(1)
            c = input("Nhap khoang cach di thang: ")
            self.goal.x = self.currentPose.x + int(c)*math.cos(self.currentPose.phi)
            self.goal.y = self.currentPose.y + int(c)*math.sin(self.currentPose.phi)
            self.goal.phi = self.currentPose.phi
            self.move_base_client()
            rospy.loginfo("Publish goal: {}".format([self.goal.x, self.goal.y, self.goal.phi]))

    def robotPoseCB(self, msg):
        self.currentPose.x = msg.position.x
        self.currentPose.y = msg.position.y
        quaternion = (  msg.orientation.x,
                        msg.orientation.y,
                        msg.orientation.z,
                        msg.orientation.w)
        euler = euler_from_quaternion(quaternion)
        self.currentPose.phi = euler[2]

    def amclPoseCB(self, msg):
        self.currentPose.x = msg.pose.pose.position.x
        self.currentPose.y = msg.pose.pose.position.y
        quaternion = (
                        msg.pose.pose.orientation.x,
                        msg.pose.pose.orientation.y,
                        msg.pose.pose.orientation.z,
                        msg.pose.pose.orientation.w)
        euler = euler_from_quaternion(quaternion)
        self.currentPose.phi = euler[2]

    def move_base_client(self):
        client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        client.wait_for_server()
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.position.x = self.goal.x
        goal.target_pose.pose.position.y = self.goal.y
        quat = quaternion_from_euler(0, 0, self.goal.phi)
        goal.target_pose.pose.orientation.x = quat[0]
        goal.target_pose.pose.orientation.y = quat[1]
        goal.target_pose.pose.orientation.z = quat[2]
        goal.target_pose.pose.orientation.w = quat[3]

        client.send_goal(goal)
        wait = client.wait_for_result()
        if not wait:
            rospy.logerr("Action server not available")
            rospy.signal_shutdown("Action server not available")
        else:
            return client.get_result()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Lack of robot pose type. ... pub_goal.py amcl or robot_pose")
        exit()
    else:
        pubGoal = PublishGoalTest(sys.argv[1])