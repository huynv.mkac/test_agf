#! /usr/bin/env python
from numpy import linalg
import numpy as np
import rospy
from tf.transformations import quaternion_from_euler
from math import pi as PI

# A = (0,0)
# B = (0,5)
# C = (2,2)

# A = np.asarray(A)
# B = np.asarray(B)
# C = np.asarray(C)

# dis = linalg.norm(np.cross(B-A, A-C)/linalg.norm(B-A))
# print(dis)
odom_quaternion = quaternion_from_euler(0.0, 0.0, PI)
print(odom_quaternion)