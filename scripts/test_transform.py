#!/usr/bin/env python
import rospy
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import tf, tf2_ros
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TransformStamped, PointStamped
from math import pi as PI

LASER_BASE_X = 1.060
LASER_BASE_Y = 0.0
LASER_BASE_Z = 1.311
LASER_BASE_YAW = PI

def transformPoint():
    laser_point = PointStamped()
    laser_point.header.frame_id = "base_laser"
    laser_point.header.stamp = rospy.Time()

    laser_point.point.x = 1.0
    laser_point.point.y = 0.2
    laser_point.point.z = 0

    base_point = PointStamped()
    ls = tf.TransformListener()
    base_point = ls.transformPoint("/base_link", laser_point)
    # (tran, rot) = ls.lookupTransform("base_link", "base_laser", rospy.Time(0))
    rospy.loginfo(  "base_laser: %.2f %.2f %.2f --> base_link: %.2f %.2f %.2f at time"%(
                    laser_point.point.x, laser_point.point.y, laser_point.point.z,
                    base_point.point.x, base_point.point.y, base_point.point.z)
                    )

if __name__ == "__main__":
    rospy.init_node("test_transform")
    while not rospy.is_shutdown():
        br = tf.TransformBroadcaster()
        br.sendTransform((1, 0, 0), (0, 0, 0, 1), rospy.Time(0), "base_laser", "base_link")
        transformPoint()
        rospy.sleep(1)
