#!/usr/bin/env python

import sys
import matplotlib.pyplot as plt
from math import sqrt
from operator import itemgetter
from numpy import linalg
import numpy as np

folderPath = "../results/"
fileTime = ""


def read_data(path, num_row):
    cnt = 0
    ret = [[] for _ in range(num_row)]
    with open(path, 'r') as f:
        while True:
            line = f.readline()
            if line.endswith('\n'):
                line = line[:-1]
            if cnt == 0:
                fileTime = line
            if cnt > 1:
                data = line.split(" ")
                if num_row > len(data):
                    # print("Error! num_row: {}, len {}".format(num_row, len(data)))
                    break

                for i in range(num_row):
                    ret[i].append(float(data[i]))

                if (not line) or ("STOP" in line):
                    break
            cnt += 1
    return fileTime, ret

def distance(A, B):
    return sqrt((A[0] - B[0])**2 + (A[1] - B[1])**2)



if __name__ == "__main__":
    if len(sys.argv) > 1:
        if (sys.argv[1] == "--help" or sys.argv[1] == "-H"):
            print("""(1) ./show_plt.py: Ve do thi cho ket qua moi nhat `lastest*`\n(2) ./show_plt.py <time>: Ve do thi cho ket qua cua file log co time tuong ung""")
            exit()
        else:
            fileTime, plan = read_data(folderPath + sys.argv[1] + "_plan.txt", 2)
            fileTime, log = read_data(folderPath + sys.argv[1] + "_log.txt", 7)
    else:
        fileTime, plan = read_data(folderPath + "lastest_plan.txt", 2)
        fileTime, log = read_data(folderPath + "lastest_log.txt", 7)
    log_odom = []
    for i in range(len(log[0])):
        log_odom.append([log[5][i], log[6][i]])

    log_plan = []
    for i in range(len(plan[0])):
        log_plan.append([plan[0][i], plan[1][i]])

    err = []
    for pose_odom in log_odom:
        err_tmp = []
        for pose_plan in log_plan:
            err_tmp.append(distance(pose_odom, pose_plan))

        min_idx = min(enumerate(err_tmp), key=itemgetter(1))[0]
        if(min_idx == len(log_plan) - 1):
            min_idx = min_idx - 2

        C = (pose_odom[0], pose_odom[1])
        C = np.asarray(C)
        A = (log_plan[min_idx][0], log_plan[min_idx][1])
        A = np.asarray(A)
        B = (log_plan[min_idx+1][0], log_plan[min_idx+1][1])
        B = np.asarray(B)
        # ct tinh khoang cach tu diem C toi duong thang duoc tao boi diem A,B
        dis = linalg.norm(np.cross(B-A, A-C)/linalg.norm(B-A))
        # print(dis)
        err.append(dis)

    goal_err = distance(log_odom[len(log_odom)-1], log_plan[len(log_plan)-1])
    goal_err = round(goal_err, 3)
    min_err = round(min(err), 3)
    max_err = round(max(err), 3)
    avg_err = round(sum(err)/len(err), 3)
    title = "Min err: {} - Max err: {} - Avg err: {} - Goal err: {}".format(min_err, max_err, avg_err, goal_err)
    plt.figure()
    plt.plot(err, label="traj_err")
    # plt.scatter(log[0], err)
    plt.title(title)
    plt.legend()
    figName = folderPath + fileTime + "_trajErr.png"
    plt.savefig(figName)
    plt.show()
    print("Saved figure in: {}".format(figName))