import math

def standardizedAngle(phi):
    """standardizedAngle(phi): Chuan hoa goc phi tu [-pi, pi] sang [0, 2pi]

    Args:
        phi (rad): Goc can chuan hoa

    Returns:
        phi: goc sau khi chuan hoa
    """
    while (phi > 2*math.pi or phi < 0):
        if(phi > 2*math.pi):
            phi -= 2*math.pi
        if(phi < 0):
            phi += 2*math.pi
    return phi

class RBPose:
    """RBPose(x,y,phi)
        phi: [0,2*pi]
    """
    def __init__(self, x=0, y=0, phi=0):
        self.x = x
        self.y = y
        # self.phi = standardizedAngle(phi)
        self.phi = phi

class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

class Line:
    """Line: ax + by + c = 0
    """
    def __init__(self, a = 0, b = 0, c = 0):
        self.a = a
        self.b = b
        self.c = c

def lineFromPose(pose):
    """Line from Pose

    Args:
        pose ([type]): [description]
    Return:
        line: ax + by + c = 0
    """
    line = Line()
    if pose.phi == math.pi/2 or pose.phi == -math.pi/2:
        line.a = 1
        line.b = 0
        line.c = -pose.x
    else:
        line.a = - math.tan(pose.phi)
        line.b = 1
        line.c = - (pose.y - math.tan(pose.phi)*pose.x)

    return line

def findI(A, B):
    """findI(A, B): Find interaction point I of two line via Pose A and Pose B

    Args:
        A (RBPose): line 1
        B (RBPose): line 2

    Returns:
        Point: Point I(Ix, Iy)
    """
    I = Point()
    a1 = math.tan(A.phi)
    a2 = math.tan(B.phi)
    b1 = A.y - math.tan(A.phi)*A.x
    b2 = B.y - math.tan(B.phi)*B.x

    I.x = (b2 - b1)/(a1 - a2)
    I.y = (a1*b2 - a2*b1)/(a1 - a2)
    return I

def distance(A, B):
    return math.sqrt((A.x - B.x)**2 + (A.y - B.y)**2)

def cos2Vector(u,v):
    """cos2Vector(u,v): Calculate cos of angle bitween two vector: u, v

    Args:
        u ([ux, uy]):
        v ([vx, vy]):

    Returns:
        float: cos(u, v)
    """
    # u: A(xA, yA), B(xB, yB) = (xB-xA), (yB-yA)
    # v: C(xC, yC), D(xD, yD) = (xD-xC), (yD-yC)
    dot_uv = (u[0]*v[0] + u[1]*v[1])
    dis_u = math.sqrt(u[0]**2 + u[1]**2)
    dis_v = math.sqrt(v[0]**2 + v[1]**2)
    return dot_uv/(dis_u*dis_v)

def ptb2(a,b,c):
    """Giai phuong trinh bac 2:
        ax^2 + bx + c
    Args:
        a (float): [description]
        b (float): [description]
        c (float): [description]
    Returns:
        [ret, x1, x2]:  ret = 0 if no solution (delta < 0)
                        x1, x2
    """

    delta = b**2 - 4*a*c
    if delta < 0:
        print("delta: {}".format(delta))
        return [0, 0, 0]
    else:
        x1 = (-b + math.sqrt(delta))/(2*a)
        x2 = (-b - math.sqrt(delta))/(2*a)
        return [1, x1, x2]

# def findIx(lineAA, R, Phi, I, A):
import sys
def findIx(A, R, Phi, I):
    """Tim toa do Ia, Ib (diem bat dau va ket thuc cung tron)

    Args:
        A (RBPose): vector A
        R (float): Ban kinh cung tron
        Phi (float, rad): goc cung tron
        I (Point): Giao diem I

    Returns:
        Point: Toa do diem Ix
    """
    lineAA = lineFromPose(A)
    print("Line: {}".format([lineAA.a, lineAA.b, lineAA.c]))
    Ix = Point()
    if lineAA.b != 0:
        a1 = -lineAA.a/lineAA.b
        b1 = -lineAA.c/lineAA.b
        tmp_a = a1**2 + 1
        tmp_b = (-2*I.x - 2*a1*I.y + 2*a1*b1)
        tmp_c = I.x**2 + (b1-I.y)**2 - (R*math.tan(Phi/2))**2
        print("size of tmp_c: {}".format(sys.getsizeof(tmp_c)))
        print("ptb2: {} {} {}".format(tmp_a, tmp_b, tmp_c))
        ret,x1,x2 = ptb2(tmp_a, tmp_b, tmp_c)
        if ret == 0:
            print("Cannot find x1, x2")
            # exit()

        y1 = a1*x1 + b1
        y2 = a1*x2 + b1
        # print("[findIx] b!=0")

    else: # lineAA.b == 0
        print("b=0")
        y1 = I.y + math.sqrt((R*math.tan(Phi/2))**2 - (-lineAA.c/lineAA.a - I.x)**2)
        y2 = I.y - math.sqrt((R*math.tan(Phi/2))**2 - (-lineAA.c/lineAA.a - I.x)**2)
        # print("y1, y2: {}".format([y1, y2]))

        x1 = x2 = - lineAA.c/lineAA.a

    # vector_AI = [I.x - A.x, I.y - A.y]
    # print("I1: {} {}".format(x1, y1))
    # print("I2: {} {}".format(x2, y2))
    vector_IA = [A.x - I.x, A.y - I.y]
    vector_II1 = [x1 - I.x, y1 - I.y]
    # print(vector_IA)
    # print(vector_II1)
    # print("[findIx] cos2Vector = {}".format(cos2Vector(vector_IA, vector_II1)))
    if cos2Vector(vector_IA, vector_II1) > 0:
        Ix.x = x1
        Ix.y = y1
        print("cosine > 0 | {}".format(cos2Vector(vector_IA, vector_II1)))
        print("x1: {} - y1: {}".format(x1, y1))
    else:
        Ix.x = x2
        Ix.y = y2
    return Ix

def posesStepStraight(StartPoint, pose, StopPoint, step):
    P = []
    phi = pose.phi
    x0 = StartPoint.x
    y0 = StartPoint.y
    NumberPoint = int(distance(StartPoint, StopPoint)/step)
    x1 = x0 + step*math.cos(phi)
    y1 = y0 + step*math.sin(phi)

    vector_Start_P = [x1 - x0, y1 - y0]
    vector_Start_Stop = [StopPoint.x - x0, StopPoint.y - y0]
    if cos2Vector(vector_Start_P, vector_Start_Stop) > 0:
        for i in range(NumberPoint):
            x1 = x0 + step*math.cos(phi)
            y1 = y0 + step*math.sin(phi)
            P.append(RBPose(x1, y1, phi))
            x0 = x1
            y0 = y1
    else:
        for i in range(NumberPoint):
            x1 = x0 - step*math.cos(phi)
            y1 = y0 - step*math.sin(phi)
            P.append(RBPose(x1, y1, phi))
            x0 = x1
            y0 = y1

    return P

def posesStepCircle(PointStart, PoseStart, PointStop, PoseStop, I, R, Phi, step):
    P = []
    phi = PoseStart.phi
    x0 = PointStart.x
    y0 = PointStart.y
    stepAngle = step/R
    NumberPoint = int(Phi/stepAngle)
    print("[pathArc] Phi: {} | stepAngle: {} | numberStep: {}".format(Phi, stepAngle, NumberPoint))
    x1 = x0 + step * math.cos(phi+stepAngle/2)
    y1 = y0 + step * math.sin(phi+stepAngle/2)

    lineStart = lineFromPose(PoseStart)
    side_P_I = (lineStart.a * x1 + lineStart.b * y1 + lineStart.c) \
                * (lineStart.a * PointStop.x + lineStart.b * PointStop.y + lineStart.c)
    signStep = 1 if side_P_I > 0 else -1
    # signStep = -1 if standardizedAngle(PoseStop.phi) < standardizedAngle(PoseStart.phi) else 1
    print("signStep: {}".format(signStep))

    vector_Start_P = [x1 - PointStart.x, y1 - PointStart.y]
    vector_Start_I = [I.x - PointStart.x, I.y - PointStart.y]

    if cos2Vector(vector_Start_P, vector_Start_I) > 0:
        print("append 1")
        for i in range(NumberPoint):
            x1 = x0 + step * math.cos(phi + signStep*stepAngle/2)
            y1 = y0 + step * math.sin(phi + signStep*stepAngle/2)
            P.append(RBPose(x1, y1, phi))
            x0 = x1
            y0 = y1
            phi = phi + signStep*stepAngle
    else:
        print("append 2")
        signStep = - signStep
        for i in range(NumberPoint):
            x1 = x0 - step * math.cos(phi + signStep*stepAngle/2)
            y1 = y0 - step * math.sin(phi + signStep*stepAngle/2)
            P.append(RBPose(x1, y1, phi))
            x0 = x1
            y0 = y1
            phi = phi + signStep*stepAngle

    return P

if __name__ == "__main__":

    # Test standardizedAngle
    import sys
    phi = 4.5
    if(len(sys.argv) > 1):
        phi = float(sys.argv[1])
    print("before standardized: phi = {}".format(phi))
    phi = standardizedAngle(phi)
    print("After standardized: phi = {}".format(phi))
