#!/usr/bin/env python3

import matplotlib.pyplot as plt
import sys
from shutil import copyfile, copy

folderPath = "../results/"
fileTime = ""


def read_data(path, num_row):
    cnt = 0
    ret = [[] for _ in range(num_row)]
    with open(path, 'r') as f:
        while True:
            line = f.readline()
            if line.endswith('\n'):
                line = line[:-1]
            if cnt == 0:
                fileTime = line
            if cnt > 1:
                data = line.split(" ")
                if num_row > len(data):
                    print("Error! num_row: {}, len {}".format(num_row, len(data)))
                    break

                for i in range(num_row):
                    ret[i].append(float(data[i]))

                if (not line) or ("STOP" in line):
                    break
            cnt += 1
    return fileTime, ret

if __name__ == "__main__":
    fileTime, log = read_data(folderPath + "nav.txt", 3)
    N = 150
    nav = []
    # for i in range(N):
    nav.append(log[1][:N])
    nav.append(log[2][:N])

    # print(nav)
    plt.figure()
    # plt.plot(err, label="traj_err")
    plt.scatter(nav[0], nav[1])
    # plt.title(title)
    plt.legend()
    # figName = folderPath + fileTime + "_trajErr.png"
    # plt.savefig(figName)
    plt.show()
    # print("Saved figure in: {}".format(figName))