#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from std_msgs.msg import Int32MultiArray as HoldingRegister
from std_msgs.msg import Float64, String, UInt16
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from enum import Enum
import sys
import socket
from sensor_msgs.msg import LaserScan

HOST = '192.168.3.51'
PORT = 2112
receive_arr = []

class Main_Step(Enum):
    GET_CONNECTION = 0
    INIT = 1
    GET_DATA = 2
    RESET = 3
    CHECK_RESET_DONE = 4

def MakeSendData(data_str):
    data_hex = "".join("{:02x}".format(ord(c)) for c in data_str)
    data_hex = '02' + data_hex + '03'
    data_src = bytearray.fromhex(data_hex).decode()
    return data_src.encode()

def SetOperatingMode(mode):
    rospy.loginfo("SetOperatingMode: {}".format(mode))
    data_str = "sMN mNEVAChangeState" + ' ' + str(mode)
    conn.send(MakeSendData(data_str))
    rospy.sleep(0.125)
    data_receive = conn.recv(1024)
    # while not data_receive:
    #     pass
    print(data_receive)
    print("size of data_receive: {}".format(sys.getsizeof(data_receive)))
    # data_receive = conn.recv(1024)
    # print(data_receive)

    # Analyze_Receive_Data(data_receive)

def SetPositioningDataFormat(outputMode, showOptParam):
    rospy.loginfo("SetPositioningDataFormat: {} {}".format(outputMode, showOptParam))
    cmd_str = "sWN NPOSPoseDataFormat " + str(outputMode) + " " + str(showOptParam)
    conn.send(MakeSendData(cmd_str))
    rospy.sleep(0.125)
    data_received = conn.recv(1024)
    print(data_received)
    # data_received = conn.recv(1024)
    # print(data_received)


def HexToSignInt(hexTxt):
    while len(hexTxt) % 8 != 0:
        hexTxt = '0' + hexTxt
    ret = struct.unpack('>i', hexTxt.decode('hex'))[0]
    return ret

def HexToUnSignInt(hexTxt):
    return(int(hexTxt, 16))

def SendCommand(cmd, bufferSize=1024):
    conn.send(MakeSendData(cmd))
    data_receive = conn.recv(bufferSize)
    # while not data_receive:
    #     pass
    print(data_receive)

def GetPoseAndScanData(wait, mask):
    data_str = "sMN mNPOSGetData" + ' ' + str(wait) + ' ' + str(mask)
    conn.send(MakeSendData(data_str))
    # rospy.loginfo('Get pose and scan')
    data_receive = conn.recv(8192)
    while not data_receive:
        pass
    print(data_receive)
    if Analyze_Received_Data(data_receive):
        return True
    return False

def Analyze_Received_Data(d):
    temp_str = ''
    if d == None:
        return False
    if len(d) == 0:
        return False
    data = d.decode("utf-8")
    # print('Receive: %s' % data)

    items = [x.replace('\x02', '').replace('\x03', '').encode('ascii') for x in data.split('\x03\x02')]
    subitems = [x.split(' ') for x in items]
    for s in subitems:
        print(s)
        # Analize_Sopas(s)
        # Analize_Pose_Data(s)
        # if not Analize_Pose_And_Scan(s):
            # return False
    return True

def Analize_Pose_And_Scan(s):
    if len(s) < 10:
        return False
    if s[3] != '0':
        rospy.loginfo('Scan data unsuccessful')
        return False
    if s[0] == 'sAN' and s[1] == 'mNPOSGetData' and s[6] == '1':
        x = float(HexToSignInt(s[7])) / 1000
        y = float(HexToSignInt(s[8])) / 1000
        theta = float(HexToUnSignInt(s[9])) / 1000
        print("x: {}, y: {}, yaw: {}".format(round(x, 3), round(y, 3), round(yaw, 3)))
        # Publish_Odom(x, y, theta)
    if len(s) < 20:
        return False
    count = 13
    range_values = []
    if s[count] == 'DIST1':
        startAngle = float(HexToSignInt(s[count + 3])) / 1000 #16
        angleStep = float(HexToUnSignInt(s[count + 4])) / 1000 #17
        timestamp_start = HexToUnSignInt(s[count + 5]) #18
        num_data_points = HexToUnSignInt(s[count + 6]) #19
        stopAngle = startAngle + (num_data_points - 1) * (angleStep)
        startAngle -= 180
        stopAngle -= 180
        # print('Start angle: %s, Stop angle: %s, Num data: %s, Angle step: %s' %(startAngle, stopAngle, num_data_points, angleStep))
        if len(s) < num_data_points + 21:
            print('Error')
            print(s)
            return False
        for i in range(num_data_points):
            range_values.append(float(HexToSignInt(s[count + 7 + i])) / 1000)
        Publish_Scan(range_values, num_data_points, False, 'front_laser', startAngle, stopAngle)
        return True

if __name__ == "__main__":
    rospy.init_node('sickNAV350', anonymous=True)
    _mainStep = Main_Step.GET_CONNECTION

    while not rospy.is_shutdown():
        # GetPoseData(1)
        # GetSopas()
        if _mainStep == Main_Step.GET_CONNECTION:
            rospy.loginfo('Get connection')
            errorCount = 0
            normalCount = 0
            conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            conn.connect((HOST, PORT))
            print(conn.gettimeout())
            rospy.loginfo('Connected')
            _mainStep = Main_Step.INIT
        elif _mainStep == Main_Step.INIT:
            SetOperatingMode(1)
            SetPositioningDataFormat(1, 1)
            SetOperatingMode(4)
            _mainStep = Main_Step.GET_DATA
        elif _mainStep == Main_Step.GET_DATA:
            c = input("Press mode: ")
            if int(c) == 1:
                GetPoseAndScanData(1, 1)
            elif int(c) == 2:
                pass
        rospy.sleep(0.125)

