#! /usr/bin/env python

import rospy
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import tf, tf2_ros
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TransformStamped
from math import pi as PI
import math

LASER_BASE_X = 1.060
LASER_BASE_Y = 0.0
LASER_BASE_Z = 1.311
LASER_BASE_YAW = PI

def wheelOdomCallback(msg):
    wheelOdom = msg
    wheelOdom.header.frame_id = "odom"
    wheelOdom.child_frame_id = "base_link"
    wheelOdomPub.publish(wheelOdom)


def navOdomCallback(msg):
    nav_x = msg.pose.pose.position.x
    nav_y = msg.pose.pose.position.y
    nav_ox = msg.pose.pose.orientation.x
    nav_oy = msg.pose.pose.orientation.y
    nav_oz = msg.pose.pose.orientation.z
    nav_ow = msg.pose.pose.orientation.w
    nav_yaw = euler_from_quaternion([nav_ox, nav_oy, nav_oz, nav_ow])[2]
    navBaseOdom = Odometry()
    navBaseOdom.header.frame_id = "map"
    navBaseOdom.child_frame_id = "base_link"
    navBaseOdom.pose.pose.position.x = msg.pose.pose.position.x + LASER_BASE_X*math.cos(nav_yaw)
    navBaseOdom.pose.pose.position.y = msg.pose.pose.position.y + LASER_BASE_X*math.sin(nav_yaw)
    yaw = euler_from_quaternion([nav_ox, nav_oy, nav_oz, nav_ow])[2] + PI
    baseQuat = quaternion_from_euler(0,0,yaw)
    navBaseOdom.pose.pose.orientation.x = baseQuat[0]
    navBaseOdom.pose.pose.orientation.y = baseQuat[1]
    navBaseOdom.pose.pose.orientation.z = baseQuat[2]
    navBaseOdom.pose.pose.orientation.w = baseQuat[3]

    navOdomPub.publish(navBaseOdom)

if __name__ == "__main__":
    rospy.init_node("test_odom", log_level=rospy.DEBUG)
    rospy.Subscriber("/nav350laser/odom", Odometry, navOdomCallback)
    rospy.Subscriber("/odom", Odometry, wheelOdomCallback)
    navOdomPub = rospy.Publisher("odometry/nav350", Odometry, queue_size=10)
    wheelOdomPub = rospy.Publisher("odometry/wheel", Odometry, queue_size=10)
    # setupTransform()
    while not rospy.is_shutdown():
        rospy.sleep(0.025)
