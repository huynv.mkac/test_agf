#!/usr/bin/env python3

import matplotlib.pyplot as plt
import sys
from shutil import copyfile, copy

folderPath = "../results/"
fileTime = ""


def read_data(path, num_row):
    cnt = 0
    ret = [[] for _ in range(num_row)]
    with open(path, 'r') as f:
        while True:
            line = f.readline()
            if line.endswith('\n'):
                line = line[:-1]
            if cnt == 0:
                fileTime = line
            if cnt > 1:
                data = line.split(" ")
                if num_row > len(data):
                    print("Error! num_row: {}, len {}".format(num_row, len(data)))
                    break

                for i in range(num_row):
                    ret[i].append(float(data[i]))

                if (not line) or ("STOP" in line):
                    break
            cnt += 1
    return fileTime, ret

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if (sys.argv[1] == "--help" or sys.argv[1] == "-H"):
            print("""(1) ./show_plt.py: Ve do thi cho ket qua moi nhat `lastest*`\n(2) ./show_plt.py <time>: Ve do thi cho ket qua cua file log co time tuong ung""")
            exit()
        else:
            fileTime, plan = read_data(folderPath + sys.argv[1] + "_plan.txt", 2)
            fileTime, log = read_data(folderPath + sys.argv[1] + "_log.txt", 7)
    else:
        fileTime, nav = read_data(folderPath + "nav.txt", 3)
        fileTime, odom = read_data(folderPath + "odom.txt", 3)
        fileTime, odomTest = read_data(folderPath + "odomTest.txt", 3)

    plt.figure()
    # plt.plot(plan[0], plan[1], label="MkacPlan")
    # plt.plot(log[5], log[6], alpha=0.5, label="odom position")
    plt.scatter(nav[1], nav[2], s=5, marker="o", color='g', label="nav pos")
    plt.scatter(odom[1], odom[2], s=5, marker="1", color='r', label="odom pos")
    plt.scatter(odomTest[1], odomTest[2], s=5, marker="*", color='b', label="odomTest pos")
    plt.grid(True)
    plt.legend()
    # plt.show()
    # plt.savefig(folderPath + "position.png")
    # copyfile(folderPath + "vel.png", folderPath+fileTime+"_vel.png")
    # copyfile(folderPath + "position.png", folderPath+fileTime+"_position.png")
    plt.show()